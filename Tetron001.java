package victor;
import robocode.*;
import robocode.util.Utils;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Tetron001 - a robot by (Victor)
 */
public class Tetron001 extends AdvancedRobot
{
	private int direcao = 1;
	private EnemyBot inimigo = new EnemyBot();
	/**
	 * run: Tetron001's default behavior
	 */
	public void run() {
		setColors(Color.red, Color.red,Color.red); // body,gun,radar
		setAdjustRadarForGunTurn(true);
		setAdjustGunForRobotTurn(true);
		//Tudo que estiver dentro do while serah executado em paralelo
		while(true) {
			setTurnRadarRightRadians(Math.PI);
			travar();
			atirar();
			movimentar();
			execute();
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		//Sempre que detectar um inimigo, atualiza o objeto para os outros metodos saberem onde ele estah
		//Mantem sempre o foco num unico inimigo ate matar
		if (inimigo.none() || inimigo.getName().equals(e.getName())) {
			inimigo.update(e);
		}
	}
	
	public void onRobotDeath(RobotDeathEvent e) {
		if (e.getName().equals(inimigo.getName())) {
			inimigo.reset();
		}
	}
	
	public void onHitRobot(HitRobotEvent e) {
		setTurnRightRadians(e.getBearingRadians());	
		setBack(50);
	}
	
	public void onHitWall(HitWallEvent e) {
		setTurnRightRadians(e.getBearingRadians());	
		setBack(80);
	}
	
	public void travar() {
		if (!inimigo.none()) {
			//Detecta se o inimigo foi perdido de vista, ou seja
			//se o angulo que ele estava no turno anterior nao variou pro turno atual
			if (inimigo.getBearingRadians() == inimigo.getBearingRadiansOld()) {
				setTurnRadarRightRadians(Math.PI); //perdeu o inimigo de vista, procurar novamente
			} else {
				double anguloInimigo = Utils.normalRelativeAngle(getHeadingRadians() - getRadarHeadingRadians() + inimigo.getBearingRadians());
				setTurnRadarRightRadians(anguloInimigo);
			}
		}
	} 
	
	//padrao de movimento
	public void movimentar() {
		if (!inimigo.none()) {
			//Se o inimigo estiver distante, se aproxima
			if (inimigo.getDistance() > 200) {
				setTurnRightRadians(inimigo.getBearingRadians());
				setAhead(inimigo.getDistance() / 2);
			} else {
				//posiciona o corpo perpendicular ao inimigo, para facilitar esquiva
				// 90 graus = 1.5708 radianos
				setTurnRightRadians(inimigo.getBearingRadians() + 1.5708);
				//inverte a direcao a cada 20 ticks
				if (getTime() % 20 == 0) {
					direcao *= -1;
					setAhead(150 * direcao);
				}
			}
		}
	}
	
	public void atirar() {
		if (!inimigo.none()) {
			//trava a mira no inimigo
			double anguloInimigo = Utils.normalRelativeAngle(getHeadingRadians() - getGunHeadingRadians() + inimigo.getBearingRadians());
			setTurnGunRightRadians(anguloInimigo);
			//atira proporcionalmente a distancia... quanto mais perto, mais forte!
			setFire(400 / inimigo.getDistance());
		}
	}
	

public class EnemyBot extends Robot {

	private volatile String name = "";
    private volatile double bearingRadians;
    private volatile double distance;
	private volatile double bearingRadiansOld;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBearingRadians() {
        return bearingRadians;
    }

    public void setBearingRadians(double bearing) {
        this.bearingRadians = bearing;
    }
	
	public double getBearingRadiansOld() {
        return bearingRadiansOld;
    }

    public void setBearingRadiansOld(double bearing) {
        this.bearingRadiansOld = bearing;
    }

    public void reset() {
        bearingRadians = 0.0;
		bearingRadiansOld = 0.0;
        distance = 0.0;
        name = "";
    }

    public boolean none() {
        return "".equals(name);
    }

    public void update(ScannedRobotEvent e) {
        bearingRadiansOld = bearingRadians;
		bearingRadians = e.getBearingRadians();
        distance = e.getDistance();
        name = e.getName();
    }
}

}
